import Vue from 'vue'
import Vuex from 'vuex'
import http from "@/services/http";

Vue.use(Vuex)

export default new Vuex.Store({
  getters: {
    coins: state => state.coins,
    portfolios: state => state.portfolios,
    selectedPortfolio: state => state.selectedPortfolio
  },
  state: {
    coins: [],
    portfolios: [],
    selectedPortfolio: null
  },
  mutations: {
    setCoins(state, coins) {
      state.coins = coins;
    },
    setPortfolios(state, portfolios) {
      state.portfolios = portfolios;
      if (portfolios.length > 0 && !state.selectedPortfolio) {
        state.selectedPortfolio = portfolios[0];
      } else if (portfolios.length === 0) {
        state.selectedPortfolio = null;
      }
    },
    setSelectedPortfolio(state, portfolio) {
      state.selectedPortfolio = portfolio;
    }
  },
  actions: {
    fetchCoins({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/coins').then(response => {
          commit('setCoins', response.data.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    },
    fetchPortfolios({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/portfolios').then(response => {
          commit('setPortfolios', response.data.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      })
    }
  }
})
