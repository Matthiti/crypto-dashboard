import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Coins from '../views/Coins.vue'
import PortfolioCoin from "../views/PortfolioCoin";
import Forbidden from "../views/Forbidden";
import Default from "../views/layouts/Default";

Vue.use(VueRouter)

const routes = [
  {
    path: '/forbidden',
    name: 'forbidden',
    component: Forbidden
  },
  {
    path: '',
    component: Default,
    children: [
      {
        path: '/',
        name: 'home',
        component: Home
      },
      {
        path: '/coins',
        name: 'coins',
        component: Coins
      },
      {
        path: '/portfolio/coins/:coinID',
        name: 'portfolio-coin',
        component: PortfolioCoin,
        props: true
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
