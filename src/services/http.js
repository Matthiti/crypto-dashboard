import axios from 'axios';
import config from '@/config';
import store from '@/store';
import router from '@/router';

axios.defaults.baseURL = config.apiUrl;

window.addEventListener('tokenLoaded', e => {
  const token = e.detail;
  axios.defaults.headers.common["X-Authorization"] = `Bearer ${token}`;

  axios.interceptors.response.use(null, function(error) {
    if (error.response) {
      if (error.response.status === 401) {
        window.location.href = `${config.ssoUrl}?redirect=${window.location.href}`;
      } else if (error.response.status === 403 && router.currentRoute.name !== 'forbidden') {
        router.push({ name: 'forbidden' });
      }
    }

    return Promise.reject(error);
  });

  store.dispatch('fetchPortfolios');
  store.dispatch('fetchCoins');
});

export default {
  axios
}
